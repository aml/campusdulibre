// switch display of menu in responsive mode
function iconElement() {
    var x = document.getElementById("myTopnav");
    if (x.className === "topnav") {
        x.className += " responsive";
    } else {
        x.className = "topnav";
    }
}

function map(){
  var mymap = L.map('my_osm_widget_map', { /* use the same name as your <div id=""> */
  center: [ 45.782179, 4.864925], /* set GPS Coordinates */
  zoom: 17, /* define the zoom level */
  zoomControl: true, /* false = no zoom control buttons displayed */
  scrollWheelZoom: false /* false = scrolling zoom on the map is locked */
});

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoiY3lsaWEiLCJhIjoiY2poNmZwcW1mMDJ4ZTJ3cGc5dmo1NzA1NyJ9.JU-g1DpPB6jTT-tiVYcUgw', {
  maxZoom: 20, /* zoom limit of the map */
  attribution: 'Données &copy; Contributeurs <a href="http://openstreetmap.org">OpenStreetMap</a> + ' +
    '<a href="http://mapbox.com">Mapbox</a> | ' +
    '<a href="https://creativecommons.org/licenses/by/2.0/">CC-BY</a> ' +
    'Guillaume Rouan 2016', /* set the map's caption */
    id: 'mapbox.streets' /* mapbox.light / dark / streets / outdoors / satellite */
}).addTo(mymap);

L.marker([45.782211, 4.865628]).addTo(mymap); /* set your location's GPS Coordinates : [LAT,LON] */

var popup = new mapboxgl.Popup({closeOnClick: false})
    .setLngLat(45.782179, 4.864925)
    .setHTML('Bâtiment Nautibus <br> Longitude :  4.864818 <br> Latitude : 45.782179')
    .addTo(map);
}

function handleClick() {

  let btn = document.getElementById('btn');
  let div = document.getElementById('conteneur');
  btn.addEventListener('click', function(event){
    if (div.classList.contains('is-hidden')) {
      div.classList.remove('is-hidden');
      btn.innerHTML = "-";
    } else {
      div.classList.add('is-hidden');
      btn.innerHTML = "+";
    }
  });
}
